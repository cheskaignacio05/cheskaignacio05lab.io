---
layout: post
title:  "My Projects!"
date:   2022-05-31 14:50:14 -0300
categories: update
---
# First Semester Projects
- [Embedded Exercises][Embedded Exercises]
- [Programming Exercises][Programming Exercises]
- [Semester Project][Semester Project]

# Second Semester Projects
 - [Rover Project][Rover project]
 - [Data center project][Data center Project]
 - [Smart City][Smart City]
 - [Networking][Networking]


[Embedded Exercises]:   https://gitlab.com/21a-itt1-embedded-exercises/cheska-ignacio-embedded-exercises
[Programming Exercises]:    https://gitlab.com/21a-itt1-programming-exercises/cheska_ignacio_programming_exercises
[Semester Project]:     https://gitlab.com/ucl-itt-21-04
[Rover project]:    https://gitlab.com/group4_project/biohazard_rover_project
[Data center Project]:  https://gitlab.com/group4_project/data_center
[Smart City]:   https://gitlab.com/group4_project/embedded_project
[Networking]:   https://gitlab.com/cheskaignacio05/Networking
